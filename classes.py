class User:
    def __init__(self):
        self.username = "Dunarr"
        self.__password = "super-secret"

    def say_hello(self, nom="John"):
        self.username = ""
        print(f"Salut {nom}")

    def display_pass(self):
        print(self.__password)


class Editor:
    def write_atricle(self):
        print("article ecrit")


class Admin(User, Editor):
    def test_method(self):
        self.write_atricle()



my_user = User()
my_user.say_hello(name="William")
print(my_user.username)
my_user.display_pass()
